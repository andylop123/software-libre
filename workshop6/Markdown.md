
<h1 align="center">Workshop 6</h1>

##### Table of Contents
[Bash scripting](#bash)  

[Cronjobs](#cron)

<a name="bash"/>

### Bash scripting
```no-highlight
En esta parte como estapa inicial creamos un documento con el comando mkdir sayhello.sh luego le dimos permisos con chmod u+x sayhello.sh una vez echo esto procedimos a editar el archivo de la siguiente manera 

Firts
if [ -z "$1" ]; then
    echo "Hola";
else
    echo "Hola $1";
fi

while [ -z `echo $bir | grep  "^[0-9]*$"`  ]; do
printf "Año en que nacio?"
read bir; 


if [ -z `echo $bir | grep  "^[0-9]*$"` ]; then
echo "ingrese un numero ";
fi
done;

luego procedimos a correr el archivo pero en mi caso no me funciono de la manera dada en el taller entonces procedi hacerlo con sh sayhello.sh luego de esto procedimos con el fuerte del taller el cual es crear otro archivo donde vamos hacer respaldo de la base de datos dentro del home de vagrant creamos una carpeta la cual llamamos backups y dentro otra carpeta llamada lfts.isw811.xyz y donde vamos almacenar los backups de la base de datos del home de vagrant crearluego creamos un archivo backup.sh en el home donde agregamos lo siguiente 



#!/bin/bash
clear


echo "starting backup..."

site="lfts.isw811.xyz"
directory="/home/vagrant/backups/${site}/"
datetime=$(date +"%Y%m%d_%h%M%s")
database="laravel"
username="laravel"
password="secret"

if [ -z "$1" ]; then
    filename=" ${site}_${datetime}.sql"
else
    filename="${site}_${1}.sql"
fi

mkdir -p $directory
 mysqldump $database > "${directory}${filename}" -u $username --password=$password


echo "compressing backup..."
cd $directory

tar cvfz "${filename}.tar.gz" "${filename}" 

echo "deleting temporary files..."

rm -r $filename


luego lo provamos con el comando sh backup.sh y nos crea backups en la carpeta destinada.

```


<a name="cron"/>

### Cronjobs
```no-highlight
En esta apartado hicimos una copia del archivo backup.sh dentro de la carpeta lfts.isw811.xyz donde luego ejecutamos el comando crontab -e en este caso yo solepcione la opción 2 ya que estoy mas familiarizado con nano y luego configuramos el tiempo a nuesta manera en este caso yo lo coloque de la siguiente manera * * *  * 4 /home/vagrant/backups/lfts.isw811.xyz/backup.sh
```






