<h1 align="center">Workshop 7</h1>

### Challenge 1
```no-highlight
/* Recupere el código (id) y la descripción (type_name) de los tipos de
movimiento de inventario (inventory_transaction_types).  */


select id as `codigo`, type_name as `Despcription` from  inventory_transaction_types;
```
![Challenge1](./IMG/Challenge1.jpg "Challenge 1")


### Challenge 2
```no-highlight
/* Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.
 */

select p.product_code as `Codigo`, p.product_name as `Productp`, sum(o.quantity) as ` cantidad`
 from order_details o join products p on p.id = o.product_id group by p.id
 order by sum(o.quantity) desc limit 10;

```
![Challenge2](./IMG/Challenge2.jpg "Challenge 2")
/* Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.
Deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades.
 */

select p.product_code as `Codigo`, p.product_name as `Product`,round(sum(o.quantity),2) as `Count` from order_details o join products p on o.product_id = p.id
group by p.id order by `Count` desc

### Challenge 3
```no-highlight

    

```
![Challenge3](./IMG/Challenge3.jpg "Challenge 3")


### Challenge 4
```no-highlight
/* Recupere el monto total (invoices, orders, order_details, products) y la
cantidad de facturas (invoices) por vendedor (employee). Debe
considerar solamente las ordenes con estado diferente de 0 y
solamente los detalles en estado 2 y 3, debe utilizar el precio
unitario de las lineas de detalle de orden, no considere el descuento,
no considere los impuestos, porque la comisión a los vendedores se
paga sobre el precio base
 */

select count(1) as `cantidad`,
e.first_name as `vendedor`,
round((od.quantity * od.unit_price),2) as `monto`
	from order_details od
    join orders o on o.id = od.order_id
    join invoices i on i.order_id = od.id
    join products p on p.id = od.product_id
    join employees e on e.id = o.employee_id
    where o.status_id != 0 and od.status_id in (2,3)
    group by e.id
    order by `cantidad` desc, `monto` desc;
    
    

```
![Challenge4](./IMG/Challenge4.jpg "Challenge 4")

### Challenge 5
```no-highlight
/* Recupere los movimientos de inventario del tipo ingreso. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente el tipo de movimiento
1 (transaction_type) como ingreso.

Debe agrupar por producto (inventory_transactions.product_id) y
deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades
ingresadas.
 */
desc inventory_transactions;
select p.product_code as `codigo` , p.product_name as `Producto`, sum(i.quantity) as `cantidad` 
from inventory_transactions i join products p on p.id = i.product_id where i.transaction_type = 1  
group by i.product_id
    
    

```
![Challenge5](./IMG/Challenge5.jpg "Challenge 5")


### Challenge 6
```no-highlight


/* Recupere los movimientos de inventario del tipo salida. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente los tipos de
movimiento (transaction_type) 2, 3 y 4 como salidas.

Debe agrupar por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto
(product_name) y la cantidad de unidades que salieron.

 */
desc inventory_transactions;
select p.product_code as `codigo` , p.product_name as `Producto`, sum(i.quantity) as `cantidad` 
from inventory_transactions i join products p on p.id = i.product_id where i.transaction_type = 2 or i.transaction_type=3 or i.transaction_type=4  
group by i.product_id
    

```
![Challenge6](./IMG/Challenge6.jpg "Challenge 6")


### Challenge 7
```no-highlight


/* Genere un reporte de movimientos de inventario
(inventory_transactions) por producto (products), tipo de transacción y
fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas
fechas).
Debe incluir como mínimo el código (product_code), el nombre del
producto (product_name), la fecha truncada
(transaction_created_date), la descripción del tipo de movimiento
(type name) y la suma de cantidad (quantity) 
 */

SELECT p.product_code AS Código,
            p.product_name AS Producto,
            itt.type_name AS Tipo,
            DATE_FORMAT(it.transaction_created_date, "%d/%m/%Y") AS Fecha,
            SUM(it.quantity) AS Cantidad
    FROM inventory_transactions it
    JOIN products p
        ON it.product_id = p.id
    JOIN inventory_transaction_types itt
        ON itt.id = it.transaction_type
    WHERE it.transaction_created_date
    BETWEEN '2006/03/22' AND '2006/03/24'
    GROUP BY p.product_name , itt.type_name DESC, it.transaction_created_date ;

```
![Challenge7](./IMG/Challenge7.jpg "Challenge 7")



### Challenge 8
```no-highlight

/* 
Genere la consulta SQL para un reporte de inventario, tomando como base todos los
movimientos de inventario (inventory_transactions), considere los tipos de movimiento
(transaction_type) 2, 3 y 4 como salidas y el tipo 1 como ingreso. 
Este reporte debe estar agrupado por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto (product_name) y
la sumarización de ingresos, salidas y la cantidad disponible en inventario (diferencia
de ingresos - salidas)
 */




SELECT p.product_code AS Código,
        p.product_name AS Producto,
        IF(it.transaction_type = 1, it.quantity, "" ) AS Ingresos,
        IF(it.transaction_type = 2 or 3 or 4, sum(it.quantity)-it.quantity, "") as Salidas,
        IF(sum(it.quantity)-it.quantity - it.quantity < 0, (sum(it.quantity)-it.quantity - it.quantity) * -1, sum(it.quantity)-it.quantity - it.quantity) as Disponible
    FROM inventory_transactions it
    JOIN products p
    ON it.product_id = p.id
    GROUP BY it.product_id;


```
![Challenge8](./IMG/Challenge8.jpg "Challenge 8")