
/* Genere un reporte de movimientos de inventario
(inventory_transactions) por producto (products), tipo de transacción y
fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas
fechas).
Debe incluir como mínimo el código (product_code), el nombre del
producto (product_name), la fecha truncada
(transaction_created_date), la descripción del tipo de movimiento
(type name) y la suma de cantidad (quantity) 
 */

SELECT p.product_code AS Código,
            p.product_name AS Producto,
            itt.type_name AS Tipo,
            DATE_FORMAT(it.transaction_created_date, "%d/%m/%Y") AS Fecha,
            SUM(it.quantity) AS Cantidad
    FROM inventory_transactions it
    JOIN products p
        ON it.product_id = p.id
    JOIN inventory_transaction_types itt
        ON itt.id = it.transaction_type
    WHERE it.transaction_created_date
    BETWEEN '2006/03/22' AND '2006/03/24'
    GROUP BY p.product_name , itt.type_name DESC, it.transaction_created_date ;