/* Recupere los movimientos de inventario del tipo ingreso. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente el tipo de movimiento
1 (transaction_type) como ingreso.

Debe agrupar por producto (inventory_transactions.product_id) y
deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades
ingresadas.
 */
desc inventory_transactions;
select p.product_code as `codigo` , p.product_name as `Producto`, sum(i.quantity) as `cantidad` 
from inventory_transactions i join products p on p.id = i.product_id where i.transaction_type = 1  
group by i.product_id
