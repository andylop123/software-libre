/* Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.
 */

select p.product_code as `Codigo`, p.product_name as `Productp`, sum(o.quantity) as ` cantidad`
 from order_details o join products p on p.id = o.product_id group by p.id
 order by sum(o.quantity) desc limit 10;
