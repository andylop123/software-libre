
/* 
Genere la consulta SQL para un reporte de inventario, tomando como base todos los
movimientos de inventario (inventory_transactions), considere los tipos de movimiento
(transaction_type) 2, 3 y 4 como salidas y el tipo 1 como ingreso. 
Este reporte debe estar agrupado por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto (product_name) y
la sumarización de ingresos, salidas y la cantidad disponible en inventario (diferencia
de ingresos - salidas)
 */




SELECT p.product_code AS Código,
        p.product_name AS Producto,
        IF(it.transaction_type = 1, it.quantity, "" ) AS Ingresos,
        IF(it.transaction_type = 2 or 3 or 4, sum(it.quantity)-it.quantity, "") as Salidas,
        IF(sum(it.quantity)-it.quantity - it.quantity < 0, (sum(it.quantity)-it.quantity - it.quantity) * -1, sum(it.quantity)-it.quantity - it.quantity) as Disponible
    FROM inventory_transactions it
    JOIN products p
    ON it.product_id = p.id
    GROUP BY it.product_id;
