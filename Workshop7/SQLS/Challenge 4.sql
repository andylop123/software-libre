/* Recupere el monto total (invoices, orders, order_details, products) y la
cantidad de facturas (invoices) por vendedor (employee). Debe
considerar solamente las ordenes con estado diferente de 0 y
solamente los detalles en estado 2 y 3, debe utilizar el precio
unitario de las lineas de detalle de orden, no considere el descuento,
no considere los impuestos, porque la comisión a los vendedores se
paga sobre el precio base
 */

select count(1) as `cantidad`,
e.first_name as `vendedor`,
round((od.quantity * od.unit_price),2) as `monto`
	from order_details od
    join orders o on o.id = od.order_id
    join invoices i on i.order_id = od.id
    join products p on p.id = od.product_id
    join employees e on e.id = o.employee_id
    where o.status_id != 0 and od.status_id in (2,3)
    group by e.id
    order by `cantidad` desc, `monto` desc;
    
    
