/* Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.
Deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades.
 */

select p.product_code as `Codigo`, p.product_name as `Product`,round(sum(o.quantity),2) as `Count` from order_details o join products p on o.product_id = p.id
group by p.id order by `Count` desc