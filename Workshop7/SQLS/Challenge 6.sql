

/* Recupere los movimientos de inventario del tipo salida. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente los tipos de
movimiento (transaction_type) 2, 3 y 4 como salidas.

Debe agrupar por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto
(product_name) y la cantidad de unidades que salieron.

 */
desc inventory_transactions;
select p.product_code as `codigo` , p.product_name as `Producto`, sum(i.quantity) as `cantidad` 
from inventory_transactions i join products p on p.id = i.product_id where i.transaction_type = 2 or i.transaction_type=3 or i.transaction_type=4  
group by i.product_id


