
##### Tabla de Contenidos



l



[Instalación de Virtualbox](#VB)  
[Instalación de Vagrant](#VA)  
[Instalación de los programas para servidor web (Apache2, PHP, MySQL y librerías)](#lists)  
[Prueba de sitio por defecto desde máquina anfitriona](#links)  
[Creación de dos sitios con Laravel](#images)  
[Configuración de VHosts para hospedar varios sitios](#code)  
[Configuración de archivos hosts para simular la resolución del dominio](#tables)  
[Creación de base de datos para Laravel](#blockquotes)  



<a name="VB"/>

## Instalación de Virtualbox

Como primer paso tenemos la instalación de Oracle VM VirtualBox por lo cual vamos acceder al sitio y descargamos el archivo para su sistema operativo y luego lo ejecutamos.


![Oracle_Site](/uploads/02f9141f964673eb00af29bbd9a5a8df/Oracle_Site.jpg)
![OVB](/uploads/c123013b3281a9967b35adb14936a34d/OVB.jpg)


<a name="VA"/>

## Instalación de Vagrant


Cuando tengamos instalado Oracle Virtual Box procederemos a ingresar al sitio de Vagrant para poder instalar el archivo para nuestro sistema operativo. Luego de una completa instalación del Vagrant vamos a cajas y buscamos bullseye y ingresamos en la mas reciente una vez dentro abrimos la terminal e ingresamos el comando vagrant add box debian/bullseye64 luego de estos el comando vagrant init debian/bullseye64 , una vez introducido dicho comando editamos el archivo vagrantfile con el comando code vagrant file una vez dentro desmarcamos la linea donde esta la red privada y por ultimo el comando vagrant up una vez finalizado, luego de esto iniciamos la maquina vagrant hay múltiples formas para hacerlo, pero en este caso yo use el comando vagrant ssh que fue el que me funciono de manera correcta

![Vagrant_site](/uploads/11904689d340c95b1f623518894550c4/Vagrant_site.jpg)
![vagrant_install](/uploads/dabe9588f851c3d159fb8a7bd8211a2c/vagrant_install.jpg)

![Vagrant](/uploads/6a472f4c412885af3ffe6d4122a11e47/Vagrant.jpg)
![vagrant_dentro](/uploads/ffd4ecb11ddbee42629c1dcbf7da70e9/vagrant_dentro.jpg)

<a name="lists"/>

## Instalación de los programas para servidor web (Apache2, PHP, MySQL y librerías)
Luego de iniciar la maquina una vez dentro procederemos a crear un nuevo usuario con el comando sudo add user laravel (laravel es el nombre que le vamos a definir) luego del usuario laravel ser creado agregamos al usuario laravel al grupo sudo con el comando sudo gpasswd -a laravel sudo, una vez agregado procedemos a instalar apache 2 , php , mysql y demas librerias pero antes de instalar 
escribimos el comando sudo apt-get update para actualizar las librerias, luegom de actualizadas ejecutamos el comando sudo apt-get install apache2 php7.4 mariadb-server mariadb-client 
![laravel](/uploads/7180bcb02675ec6aeb0752be6a4f206d/laravel.jpg)
![install_php.etc](/uploads/4d79c16820a72b0dd89b76b11390fe07/install_php.etc.jpg)

<a name="links"/>


## Prueba de sitio 
Una vez instalado apache nos vamos a nuestra maquina anfitriona a configurar en los host la dirección del web server y agregamos la ip 192.168.33.10 con el nombre webserver y luego probamos en el navegador ingresando la IP
![webserver](/uploads/28adae062335627d4290ac459acacb43/webserver.jpg)



<a name="images"/>

## Creación de dos sitios con Laravel
Para la creación de sitios laravel primero tenemos que recurrir a la instalación de composer para eso ingresamos a la pagina y buscamos la instalación, luego de tener instalado composer procederemos a las instalación de los sitios laravel ingresando los siguientes comandos:

-composer global require laravel/installer

-laravel new laravel.isw811.xyz

y la misma historia para el otro sitio 


-composer global require laravel/installer

-laravel new blog.isw811.xyz

![laravel_proyect](/uploads/a664a05173a9805f34d5b033833758c1/laravel_proyect.jpg)


<a name="code"/>

## Configuración de VHosts para hospedar varios sitios

Dentro de la maquina procedemos a crear dos archivos con el comando touch laravel.isw811.xyz.conf blog.isw811.xyz.conf
luego de creados procedemos a editarlos con la información correspondiente para un vhost, luego de editados procedemos a moverlos con el comando sudo mv *.conf /rtc/apache2/sites-avaible/  luego procedemos activarlos con los módulos del comando sudo a2enmod vhost_alias rewrite ssl y luego reiniciamos apache con systemctl restart apache

![vHost](/uploads/bc8a4fdbca8657a9d8214f317999c198/vHost.jpg)


<a name="tables"/>

## Configuración de archivos hosts para simular la resolución del dominio

Luego procedemos a configurar los host de nuestra maquina anfitrión con la IP y los nombres de los sitios 

 ![blog](/uploads/73b18e56262d54c9fd9f2eab57eb051a/blog.jpg)
![site](/uploads/5f615ce3938311db3ad892a19cd1092f/site.jpg)




<a name="blockquotes"/>

## Creación de base de datos para Laravel

con sudo mysql entramos a mariaDB una vez dentro creamos la base de datos y luego un usuario y le asignamos sus roles una vez finalizado nos vamos a los sitios de laravel  a configurar en el archivo .env los datos
![db](/uploads/d2ab3bbf4054ea4fa31ca5e2d4300fa3/db.jpg)